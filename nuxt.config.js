export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'vue-example',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      {name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1'},
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    {src: '~/assets/scss/main.scss', mode: 'client'}
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/moment',
    '@nuxt/image',
    '@nuxtjs/dotenv',
    '@nuxtjs/fontawesome'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/style-resources',
    '@nuxt/image',
  ],

  image: {
    // provider: '~/assets/',
    // publicPath: '~/static/',
    staticFilename: '[publicPath]/images/[name]-[hash][ext]',
    // domains: [process.env.NODE_ENV !== 'production' ? 'vacilando-admin.kaiyouit.com' : 'admin.vacilando.co.jp'],
  },
  fontawesome: {
    component: 'Fa',
    suffix: false,
    icons: {
      solid: true,
      brands: true,
    },
},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    filenames: {
      img: ({isDev}) => isDev ? '[path][name].[ext]' : 'img/[name].[contenthash:7].[ext]',
    },
  },
}
